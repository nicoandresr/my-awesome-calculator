const server = require('./server')
const request = require('supertest')
const { sum } = require('./testHelper')

test('Get root app', done => {
  request(server)
    .get('/')
    .expect(200)
    .then(response => {
      expect(response).toBeDefined()
      expect(response.text).toBeDefined()
      expect(response.text).toContain('<title>My Awesome Calculator</title>')
      done()
    })
})

test('Get index.js', done => {
  request(server)
    .get('/bundle.js')
    .expect(200, done)
})

test('Get favicon.ico', done => {
  request(server)
    .get('/favicon.ico')
    .expect(200, done)
})

describe('Sum endpoint', () => {
  sum(1).plus(2).equalTo(3)
//  sum(0).plus(2).equalTo(2)
 // sum(0).plus(0).equalTo(0)
 // sum(-5).plus(10).equalTo(5)
})
/*
describe('Subs endpoint', () => {
  subs(1).minus(1).equalTo(0)
  subs(1).minus(0).equalTo(1)
  subs(0).minus(-1).equalTo(-1)
  subs(-1).minus(-1).equalTo(-2)
})
*/
