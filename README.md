# My Awesome Calculator

This project is to show the features of server-side rendering with react and node js, also I use advanced concepts like react suspense, react hooks, and maybe experimental features like react servers components.

## Objetives
- do Server Side Rendering (SSR)
- use react Suspense.
- use react hooks (funtional components)
- (optional) use server components

## Next steps
My goal with this project is in two week by 30 minutes by day, develop a calculator en React, and at the end could have some practical experience using the advanced features, good points, bad points.

## Main Stack
- node js.
- express js.
- React.
- React experimental features.
- Plain CSS.
- My editor is NeoVim. (you can see some nice screenshoots :) )
- ESLint with google standard.
- Jest for unit testing (Doing TDD).

## Related articles
- [React Dom Server](https://reactjs.org/docs/react-dom-server.html#gatsby-focus-wrapper)
- [React Concurrent Mode](https://reactjs.org/docs/concurrent-mode-intro.html)
- [React Hooks](https://reactjs.org/docs/hooks-intro.html)
- [React Server Components](https://reactjs.org/blog/2020/12/21/data-fetching-with-react-server-components.html)

## features
- The calculator must be mobile first.
- The calculator must have a pretty styles.
- The calculator must be serve rendered from the server.
- Each operation must be procesed by the server in a GET operation.
- The server must to remember the last operation.

## Day 1
This day only create this repo and this Readme in the first commit
## Day 2
This day define the features and objetives, also I initialize the project with `npm init` and install jest to doing the unit testing, and write my first test!! 🎉 🎉
PD: I decided use the sufix `spec.js` to my test files.
## Day 3
Create gitignore, initialize eslint `npx eslint --init` and the first question is, what type of modules does my project use? 'import/export' or 'require/exports' the answer is ovius but, how I can write SSR with 'import/export' then I believe that the best is have separate folders for server and client because are two diferent parts Node, and React.
After that I need config .eslint to use jest in the server folder adding `"jest": true` into `"env"` in the .eslintrc.jsfile.

At least I do my first commit with the few code.🎉 🎉
## Day 4
Update the package.json to include the `server/index.js` in the main entry.
I did install `express js` and I'm seeking the correct way to test the app.
try to write some test, but I dont have idea how testing a express app.
## Day 5
I read about `supertest` that is "HTTP assertions made easy via superagent." in few words is a library to test a node api, and is very easy to use, my first test is more solid after use `supertest` 🎉 here the result:
```js
const app = require('./app')
const request = require('supertest')

test('Initialize server', done => {
  request(app)
    .get('/')
    .expect(200, done)
})
```
## Day 6
Today my goal is create the "Hello world" react component with SSR, for this I'll create the client folder with the react app and do the `eslint --init` in this folder, also we need config babel to compile the react app and could use it in the server side, I just install babel and presets
```js
npm i -D @babel/core @babel/cli @babel/preset-env @babel/preset-react
```
and create the `babel.config.json` in this file I configure the presets
```js
{
  "presets": [
    "@babel/preset-env",
    "@babel/preset-react"
  ]
}
```
finally I did modify the `package.json` to include the build script:
```js
"build": "babel client -d lib"
```
After that I created the react component `client/components/layout.js` with a single 'hello' inside a div, and run `npm run build` and SUCCESS the lib folder was created with the ES2015 react component inside.
## Day 7
Now we have mane pieces of the puzle, we have a node server, a react app, and a babel compiler, with this togheter we can build a SSR React app, lets do it, my first error, I dont have react installed yet :facepalm, then install it `npm install react@next react-dom@next`, after that I try use `ReactDOMServer` to render my forst component, here my first attempt:

```js
const Hello = require('../lib/components/hello')
...
res.send(ReactDOMServer.renderToString(Hello))
```
But got error, says that a object not is a valid react element, why? Because obviously Hello not is a valid react element, I search about this and I found that need use `React.createElement()` to solve it, then my second attempt was:
```js
res.send(ReactDOMServer.renderToString(React.createElement(Hello)))
```
But also got error that says `Element type is invalid`, ok I remember that when you use commonJS `exports/require` the default export you need do the require with `require('some-mudule').require` then I try that
```js
const Hello = require('../lib/components/hello').default
...
res.send(ReactDOMServer.renderToString(Hello))
```
And works!!, my first SSR is working now! Yeah!
## Day 8
Today I want check the `hydrate`, the react page says that: 
> is used to hydrate a container whose HTML contents were rendered by ReactDOMServer. React will attempt to attach event listeners to the existing markup.
I changed my response into server to render a HTML with a script tag where I will load the `index.js` with the react app.
## Day 9
I continue to reading about `hydrate` this method is to attach the html rendered by the server, whet I try to serve the `index.js` that contains the React App, I found an error, my project structure is:

- root
 - server: here the server code
  - index.js: `app.listen` function 
  - app.js express app config
 - client: here the client code
In this structure the server could don't access the client code because the server don't can access the client code for security reasons, then I need change the struture to:
- root: here the server code
 - index.js: `app.listen` function 
 - server.js (old server/app.js)express app config
 - client: React app code here
# Day 10
Today I'll change my project structure to solve the yesterday's issue, and I hope could do a `hydratate` succesfuly.
Doing this change I found that eslint will be regenerate in the root to check/fix `require/exports` way, and works fine, now I add a new endpoints new in the server: 

```js
app.get('/index.js', (req, res) => {
  res.status(200)
  res.sendFile(__dirname + '/lib/index.js')
})

app.get('/favicon.ico', (req, res) => {
  res.sendStatus(200)
})
```
and the test for each them

when I try to do the first render in the client I got the error
`Uncaught ReferenceError: require is not defined` the error is because the browser don't recognize the `require/exports` commonJS way, maybe I need also a webpack to bundle the js in one file
after install `webpack` (`npm i webpack -D`) I could configure it to bundle the `lib/` output into `dist/bundle.js` then I change the  script in html to:
```html
<script src='./bundle.js'></script>
```
and works but before I got a issue: `Warning: Did not expect server HTML to contain the text node "` this error was because I dont have the exactly html render by the server and the client, in the server I have:
### server:
```html
<div id="root">
  ...here my react app
</div>
```
and into the client the react app renders:
### client:
```html
<div id="root">...here my react app</div>
```
I can solve this issue changing the server render deleting the new line:
### server:
```html
<div id="root">...here my react app</div>
```
### client:
```html
<div id="root">...here my react app</div>
```
After that I got a SSR and a hydratate succesfuly, working, and this Is great!!

I need solve a last thing, I only wants do a fetch data from the server and dont repeat into the client, for achieve that I need implement a getInitialProps like `Next.js` way, this getServerProps only are executed in the server side and don't are executed into the client, avoiding repeat a api call for instance
## Day 11
Today I explore how NextJS persist the initial props trough the server side and client side, and NextJS uses a 
`<scrit id="__NEXT_DATA__" type="application/json">...data here</script>`
and after do a:
```js
const data = JSON.parse(document.getElementById('__NEXT_DATA__').text)
```
to extract the data, then I'll do the same apprach to persist the initial props between the server and the client
and works fine!! hurra!!
## Day 12
Today I'll explain how the build process works, and the concerns of some files:
### Build task
`babel client -d lib && webpack --mode=development`
this taks do two things first execute a babel process from client and put the output in lib, and after that it run webpack that takes the lib content and bundle the app and puts the result in dist folder this is ok because with the first result we can use the react code in the node js server, and with the second operation we can use the bundle in the client side.
my webpack.conf is this:

````js
const path = require('path')

module.exports = {
  entry: './lib/index.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  }
}
````
### Project structure
In the project we have the following structure:
- root directory
  - server.js: this is the server's endpoints definition
  - index.js: this is the entry point, takes the server.js and listen in a port
  - client/: this folder have the react app code
  - lib: this folder have the react app code output of babel compilation
  - dist: this folder contains the bundle generated by webpack

And thats its!
## Day 13
Today I need solve my broken unit test, I got the error 
`ReferenceError: regeneratorRuntime is not defined`
And I suspect that is because the async/await into the server.js
```js
server.get('/', async (req, res) => {
  res.status(200)
  const props = await getServerProps()
  ...
```
Then I need found the error and fixed it.
## Day 14
I just solve the broken unit test, and the solution to `regeneratorRuntime is not defined`, was modify my `babel.config.js` file:
### Before
```js
"@babel/preset-env"
```
### After
```js
[
    "@babel/preset-env",
    {
      "targets": {
        "node": "current"
      }
    }
  ]
]
```
And now the test are runing again!
#### Explanation:
The `targets` option:
> Describes the environments you support/target for your project.
then jest uses the babel configuration to run the test without this seems that jest don't some features of my code, :thinking 
## Day 15
Today I improve the `server.spec.js` test, to validate the response and find some html to match with the expected ones:
```js
test('Get root app', done => {
  request(server)
    .get('/')
    .expect(200)
    .then(response => {
      expect(response).toBeDefined()
      expect(response.text).toBeDefined()
      expect(response.text).toContain('<title>My Awesome Calculator</title>')
      done()
    })
})
```
also I installed husky to run the test before each commit, and was so easy with `husky`:
- `npm install husky`
- `npm set-script prepare "husky install" && npm run prepare`
- `npx husky add .husky/pre-commit "npm test"`
And its all before each commit the unit test are run automatically. :yep
## Day 16
Today I will create the sum and the multiply end points, and I'm reading about express js params, here the info https://expressjs.com/en/4x/api.html#req

Also I createt a helper function to do the test more easy and legibles:
```js
function sum(a) {
 return {
   plus: b => ({
     equalTo: result => {
       test(`Sum ${a} plus ${b} equal to ${result}`, done => {
         request(server)
          .get(`/api/sum/${a}/plus/${b}`)
          .send({ result })
          .expect(200, done)
       })
     }
   })
 }
}
```
then you can write test easy to read
```js
describe('Sum endpoint', () => {
  sum(1).plus(2).equalTo(3);
  sum(0).plus(2).equalTo(2);
  sum(0).plus(0).equalTo(0);
})
```

And here our husky in action:

![Husky Unit Test](./screenshots/Calculator Day 16.png)

## Day 17
Today's I'll create some other endpoints like multiply, minus and division, let's do it.
## Day 18
I need continue creating the end points, but now I known that the test are misspelled, and not are working correctly, for instance the test is with result ok, because I'm using the `send` method to say what is the response, more that, I'm not saying that these are the expected result, for that I need use the expect, some like thish:
```js
request(server)
  .get(`/api/sum/${a}/plus/${b}`)
  .send({ result }) // < -- BAD
  .expect(200, done)
})
```
the correct way is:
```js
request(server)
  .get(`/api/sum/${a}/plus/${b}`)
  .expect(200, { result }, done) <-- CORRECT
})
```
the second parameter into `expect` could be a object with the expected result.

Lets fix them!
