import * as React from 'react'
import ReactDOM from 'react-dom'

import App from './app'

const props = JSON.parse(document.getElementById('__DATA__'))

ReactDOM.hydrate(<App {...props} />, document.getElementById('root'))
