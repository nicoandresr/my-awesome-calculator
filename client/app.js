import * as React from 'react'
import Layout from './components/layout'

export default function App(props) {
  return (
    <Layout>
      <div>Hello {props.name}!</div>
    </Layout>
  )
}

export function getServerProps() {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve({ name: 'Nico' })
    }, 2000)
  })
}
