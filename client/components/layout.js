import * as React from 'react'

function Layout(props) {
  return <div>{props.children}</div>
}

export default Layout
