const server = require('../server')
const request = require('supertest')

module.exports.sum = function sum(a) {
 return {
   plus: b => ({
     equalTo: result => {
       test(`Sum ${a} plus ${b} equal to ${result}`, done => {
         request(server)
          .get(`/api/sum/${a}/plus/${b}`)
          .expect(200, { result }, done)
       })
     }
   })
 }
}

module.exports.subs = function substitution(a) {
  return {
    minus: b => ({
      equalTo: result => {
        test(`Substraction ${a} minus ${b} equal to ${result}`, done => {
          request(server)
            .get(`/api/subs/${a}/minus/${b}`)
            .expect(200, { result }, done)
        })
      }
    })
  }
}
