const React = require('react')
const ReactDOMServer = require('react-dom/server')
const express = require('express')

const { default: app, getServerProps } = require('./lib/app')

const server = express()

server.get('/bundle.js', (req, res) => {
  res.status(200)
  res.sendFile(__dirname + '/dist/bundle.js')
})

server.get('/favicon.ico', (req, res) => {
  res.sendStatus(200)
})

server.get('/api/sum/:a/plus/:b', (req, res) => {
  res.status(200)
  const a = Number.parseInt(req.params.a)
  const b = Number.parseInt(req.params.b) * -1
  const result = a - b
  res.json({ result })
})

server.get('/api/subs/:a/minus/:b', (req, res) => {
  res.status(200)
  const a = Number.parseInt(req.params.a)
  const b = Number.parseInt(req.params.b) * -1
  const result = a - b
  res.send(`{"result": ${result}}`)
})

server.get('/', async (req, res) => {
  res.status(200)
  const props = await getServerProps()
  const content = ReactDOMServer.renderToString(React.createElement(app, props))
  res.send(`<html>
    <head>
      <title>My Awesome Calculator</title>
    </head>
    <body>
      <div id="root">${content}</div>
      <script id="__DATA__" type="application/json">${JSON.stringify(props)}</script>
      <script src="./bundle.js"></script>
    </body>
  </html>
  `)
})

module.exports = server
